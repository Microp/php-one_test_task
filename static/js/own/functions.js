/**
 * Created by alexander on 18/07/2017.
 */


function relocate(link) {
    location.href = link;
}

function openMessageForm() {
    var div = document.getElementById('form_message');
    if (div.style.display == 'none') {
        div.style.display = 'block';
    } else {
        div.style.display = 'none';
    }
}
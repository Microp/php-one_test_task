<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 20/07/2017
 * Time: 23:34
 */

namespace classes\models;

use classes\DbConnection;

/**
 * new human(name, 22)
 *

 */
class Model
{
    protected static $columns;
    protected static $table_name;
    protected static $unique_columns;

    protected static $fields;


    public static function factoryFromDb($arr)
    {
    }

    static function createTable()
    {
        static::$createQuery = 'CREATE TABLE ' . static::table_name . "\n";
        static::$createQuery .= 'id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,' . "\n";
    }

    public static function getAllObjects($offset = null, $limit = null, $orderByColumns = null)
    {
        $db = DbConnection::getInstance();
        $objects = $db->select(static::$columns)->from(static::$table_name)->offset($offset)->limit($limit)->orderBy($orderByColumns)->exec('n');
        foreach ($objects as $arr) {
            yield static::factoryFromDb($arr);
        }
    }

    public static function getObjectById($search_id)
    {
        $db = DbConnection::getInstance();
        $object = $db->select(static::$columns)->from(static::$table_name)->where('id', '=', $search_id)->exec(1);
        if ($object != null) {
            return static::factoryFromDb($object);
        } else {
            return null;
        }
    }

    /**
     * @param $fields_condition_values ==== (field, =, value)
     */
    public static function getAllObjectsByFields($fields_condition_values, $offset = null, $limit = null, $orderByComumns = null)
    {
        $db = DbConnection::getInstance();
        $objects = $db->select(static::$columns)->from(static::$table_name)->where($fields_condition_values)->offset($offset)->limit($limit)->orderBy($orderByComumns)->exec('n');
        foreach ($objects as $arr) {
            yield static::factoryFromDb($arr);
        }
    }


    /**
     * Create object by constructor with known field like new Model(email = x, name = y)
     * after this find full entity with same field, but only if this record is exist and unique
     * unique is that method find only one record from DB
     */
    public function getObjectByFields()
    {
        $db = DbConnection::getInstance();
        $existing_values = [];
        foreach (static::$columns as $column) {
            if ($this->{$column} != null) {
                $existing_values[] = $column;
                $existing_values[] = '=';
                $existing_values[] = $this->{$column};
            }
        }
        $res = $db->select(static::$columns)->from(static::$table_name)->where($existing_values)->exec('n');

        if (count($res) == 1) {
            return static::factoryFromDb($res[0]);
        } else {
            return null;
        }
    }


    /**
     * Deleting record from db by id of model
     */
    public function delete()
    {
        $db = DbConnection::getInstance();

        $condition[] = 'id';
        $condition[] = '=';
        $condition[] = $this->id;

        $res = $db->deleteFrom(static::$table_name)->where($condition)->exec();
    }


    public function update()
    {
        $db = DbConnection::getInstance();

        $condition[] = 'id';
        $condition[] = '=';
        $condition[] = $this->id;

        $columnName_value = [];

        foreach (static::$columns as $column) {
            $columnName_value[] = $column;
            $columnName_value[] = $this->{$column};
        }

        $res = $db->update(static::$table_name)->set($columnName_value)->where($condition)->exec();
        return $res;
    }

    public function updateByField($field)
    {
        if ($this->id == null) {
            return false;
        }
        $db = DbConnection::getInstance();

        $condition[] = 'id';
        $condition[] = '=';
        $condition[] = $this->id;

        $columnName_value[] = $field;
        $columnName_value[] = $this->{$field};

        $res = $db->update(static::$table_name)->set($columnName_value)->where($condition)->exec();

    }

    public function create()
    {
        $bool_errors = $this->if_exist();

        if ($bool_errors == false) {

            $db = DbConnection::getInstance();
            $values = [];
            foreach (static::$columns as $column) {
                $values[] = $this->{$column};
            }

            $res = $db->insertInto(static::$table_name, static::$columns)->values($values)->exec();
            return false;
        } else {
            return $bool_errors;
        }
    }

    protected function if_exist()
    {
        $db = DbConnection::getInstance();
        foreach (static::$unique_columns as $unique_column) {
            $res = $db->select()->count('*')->from(static::$table_name)->where($unique_column, '=', $this->{$unique_column})->exec(1);
            if ($res['count'] > 0) {
                return 'This record already exist in data base field -  ' . $unique_column . '<br>';
            }
        }
        return false;
    }

    public function is_valid()
    {
        foreach (static::$fields as $field) {
            // {$field['name']} - pointer to value of instance
            // $field['x'] - pointer to static description of field which defined in static content of model
            $field['class']::verify($this->{$field['name']}, $field['length'], $field['name']);
        }
        return true;
    }

    static function objectsGetTotalCount($where)
    {
        $db = DbConnection::getInstance();
        $res = $db->select()->count('*')->from(static::$table_name)->where($where)->exec(1);
        return $res['count'];
    }

}
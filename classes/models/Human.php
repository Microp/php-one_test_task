<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 21/07/2017
 * Time: 00:30
 */

namespace classes\models;

/**
 * $human = Human::getAll()
 */

use classes\models;
use classes\models\fields;


class Human extends models\Model
{
    static $columns = ['id', 'name', 'age'];
    static $table_name = 'humans';
    static $unique_columns = ['id', 'name'];

    protected static $fields = [
        'name' => [
            'class' => fields\CharField::class,
            'name' => 'name',
            'length' => 255,
        ],
        'age' => [
            'class' => fields\IntegerField::class,
            'name' => 'age',
            'length' => 11,
        ],
    ];

    public $id;
    public $name;
    public $age;


    function __construct($name, $age, $id = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->age = $age;
    }

    public static function factoryFromDb($arr)
    {
        return new Human($arr['name'], $arr['age'], $arr['id']);
    }


    static function createTable()
    {
        $query = 'CREATE TABLE IF NOT EXISTS `humans` ( `id` int(11) NOT NULL AUTO_INCREMENT, `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL, `age` int(11) DEFAULT NULL, PRIMARY KEY (`id`) )';
        return $query;
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 20/07/2017
 * Time: 23:41
 */

namespace classes\models\fields;


class Field
{
    protected static $variable_type;

    protected static function type_match($value, $field_name)
    {
        $value_type = gettype($value);
        if ($value_type != static::$variable_type) {
            echo 'Type mismatch feield = ' . $field_name . '<br>';
            echo $value_type;
            return false;
        } else {
            return true;
        }
    }

    protected static function length_match($value, $length, $field_name)
    {
        $value_type = gettype($value);
        if ($value_type == 'integer' or $value_type == 'NULL') {
            return true;
        }
        if (strlen($value) > $length) {
            echo 'Value exceeds the corresponding length = ' . $field_name . '<br>';
            return false;
        } else {
            return true;
        }
    }

    public function verify($value, $length, $field_name)
    {
        $res = [];
        $res[] = static::length_match($value, $length, $field_name);
        $res[] = static::type_match($value, $field_name);

        foreach ($res as $bool) {
            if (!$bool) {
                return false;
            }
        }
        return true;
    }


}
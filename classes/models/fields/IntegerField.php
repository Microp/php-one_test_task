<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 20/07/2017
 * Time: 23:48
 */

namespace classes\models\fields;


class IntegerField extends Field
{
    protected static $variable_type = 'integer';


}
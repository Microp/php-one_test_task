<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 05/08/2017
 * Time: 14:38
 */

namespace classes\models\fields;


class ForeignKey extends Field
{
    protected static $variable_type = 'integer';

}
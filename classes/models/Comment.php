<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 06/08/2017
 * Time: 15:04
 */

namespace classes\models;


use classes\models\fields\CharField;
use classes\models\fields\ForeignKey;


class Comment extends Model
{
    protected static $columns = ['id', 'text', 'article_id', 'user_id'];
    protected static $table_name = 'comments';
    protected static $unique_columns = ['id'];


    protected static $fields = [
        'text' => [
            'class' => CharField::class,
            'name' => 'title',
            'length' => null,
        ],
        'article_id' => [
            'class' => ForeignKey::class,
            'name' => 'article_id',
            'length' => null,
            'reference' => ['articles', 'id'],
        ],
        'user_id' => [
            'class' => ForeignKey::class,
            'name' => 'user_id',
            'length' => null,
            'reference' => ['users', 'id'],
        ]
    ];

    public $text;
    public $article_id;
    public $user_id;
    public $id;

    public $user;
    public $article;

    function __construct($text, $article_id, $user_id = null, $id = null)
    {
        $this->text = $text;
        $this->article_id = $article_id;
        $this->user_id = $user_id;
        $this->id = $id;

        $this->user = User::getObjectById($user_id);
        $this->article = Article::getObjectById($article_id);
    }

    public static function factoryFromDb($arr)
    {
        return new static($arr['text'], $arr['article_id'], $arr['user_id'], $arr['id']);
    }

    static function createTable()
    {
        $query = 'CREATE TABLE IF NOT EXISTS `comments`
         ( `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `text` text NOT NULL,
          `article_id` int(11) unsigned NOT NULL,
          `user_id` int(11) unsigned DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `article_id` (`article_id`), KEY `user_id` (`user_id`),
          CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
          CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) 
        )';
        return $query;
    }


}
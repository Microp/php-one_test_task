<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 05/08/2017
 * Time: 14:30
 */

namespace classes\models;


use classes\DbConnection;
use classes\models\fields\CharField;
use classes\models\fields\ForeignKey;

class Article extends Model
{
    protected static $columns = ['id', 'title', 'text', 'user_id', 'temp_name'];
    protected static $table_name = 'articles';
    protected static $unique_columns = ['id'];

    protected static $fields = [
        'title' => [
            'class' => CharField::class,
            'name' => 'title',
            'length' => 255
        ],
        'text' => [
            'class' => CharField::class,
            'name' => 'text',
            'length' => null
        ],
        'user_id' => [
            'class' => ForeignKey::class,
            'name' => 'user_id',
            'length' => null
        ]

    ];

    public $title;
    public $text;
    public $user_id;
    public $id;
    public $temp_name;

    public $user;

    function __construct($title, $text, $user_id, $temp_name = null, $id = null)
    {
        $this->title = $title;
        $this->text = $text;
        $this->user_id = $user_id;
        $this->temp_name = $temp_name;
        $this->id = $id;

        $this->user = User::getObjectById($user_id);
    }

    public static function factoryFromDb($arr)
    {
        return new static($arr['title'], $arr['text'], $arr['user_id'], $arr['temp_name'], $arr['id']);
    }

    static function getObjectByUser($user_id)
    {
        $db = DbConnection::getInstance();
        $objects = $db->select(static::$columns)->from(static::$table_name)->where('user_id', '=', $user_id)->exec('n');
        foreach ($objects as $arr) {
            yield static::factoryFromDb($arr);
        }


    }


    static function createTable()
    {
        $query = 'CREATE TABLE IF NOT EXISTS `articles` 
        (`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
         `title` varchar(255) NOT NULL,
         `text` text NOT NULL,
         `user_id` int(11) unsigned DEFAULT NULL,
         `temp_name` varchar(255) DEFAULT NULL,
         PRIMARY KEY (`id`),
         KEY `user_id` (`user_id`),
         CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
         )';
        return $query;
    }
}
<?php

namespace classes;

use PDO;

class DbConnection
{
    private static $instance = null;
    private $dbconn = null;

    private $select = null;
    private $count = null;
    private $from = null;
    private $where = null;
    private $limit = null;
    private $offset = null;
    private $orderBy = null;

    private $insertInto = null;
    private $values = null;

    private $update = null;
    private $set = null;
    // where

    private $deleteFrom = null;

    // where


    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * DbConnection constructor.
     */
    private function __construct()
    {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/configs/db_conf.php';


        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        $opt = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];

        try {
            $this->dbconn = new PDO($dsn, $user, $pass, $opt);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }

    }

    function __clone()
    {
        // TODO: Implement __clone() method.
    }

    public function old_select($query)
    {
        $stmt = $this->dbconn->query($query);
        return $stmt;
    }

    public function select()
    {
        $this->select = 'SELECT ';
        if (func_num_args() > 0) {
            $vars = func_get_args();

            if (gettype($vars[0]) == 'array') {
                $vars = $vars[0];
            }

            foreach ($vars as $var) {
                if ($var == '*') {
                    $this->select .= '* ';
                    break;
                }
                $this->select .= '`' . $var . '`, ';
            }
            $this->select = substr($this->select, 0, -2) . ' ';
        }
        return $this;
    }

    public function count()
    {
        $vars = func_get_args();
        $this->count = 'COUNT(';

        foreach ($vars as $var) {
            if ($var != '*') {
                $this->count .= '`' . $var . '`';
            } else {
                $this->count .= '*';
            }
        }
        $this->count .= ') AS `count`';
        return $this;
    }

    public function from($table)
    {
        $this->from = 'FROM `' . $table . '` ';
        return $this;

    }

    public function where()
    {
        $vars = func_get_args();
        if ($vars[0] == null) {
            return $this;
        }
        $this->where = 'WHERE ';


        if (gettype($vars[0]) == 'array') {
            $vars = $vars[0];
        }

        for ($i = 0; $i < count($vars); $i += 3) {
            $this->where .= ('`' . $vars[$i] . "` " . $vars[$i + 1] . ' ');
            if (gettype($vars[$i + 2]) == 'integer') {
                $this->where .= $vars[$i + 2] . " ";
            } else {
                $this->where .= "'" . $vars[$i + 2] . "' ";
            }
            if ($vars[$i + 3] == 'AND') {
                $this->where = ' AND ';
                $i++;
            } elseif ($vars[$i + 3] == 'NO') {
                $this->where = ' NO ';
                $i++;
            }
        }
        return $this;
    }

    public function limit($count)
    {
        if ($count != null) {
            $this->limit = 'LIMIT ' . $count . ' ';
        }
        return $this;
    }

    public function offset($count)
    {
        if ($count != null) {
            $this->offset = 'OFFSET ' . $count . ' ';
        }
        return $this;
    }

    public function orderBy($column)
    {
        if ($column == null) {
            return $this;
        }
        $this->orderBy = 'ORDER BY ';

        if ($column[0] == '-') {
            $this->orderBy .= '`' . substr($column, 1) . '` DESC ';
        } else {
            $this->orderBy .= '`' . $column . '` ';
        }

        return $this;
    }

    public function exec($count = null)
    {
        if ($this->select != null) {
            $query = $this->select . $this->count . $this->from . $this->where .  $this->orderBy . $this->limit . $this->offset;
//            echo $query;
            $stmt = $this->dbconn->query($query);
            $this->clearQery();
            if ($count == null) {
                return $stmt;
            } else {
                $res = [];
                $i = 0;

                while ($row = $stmt->fetch()) {
                    if ($i < $count or $count == 'n') {
                        $res[] = $row;
                    } else {
                        break;
                    }
                    $i++;
                }
                if ($count == 1) {
                    return $res[0];
                }
                return $res;
            }
        } elseif ($this->insertInto != null) {
            $query = $this->insertInto . $this->values;
//            echo $query;
            $stmt = $this->dbconn->query($query);
            return $stmt;
        } elseif ($this->deleteFrom != null) {
            $query = $this->deleteFrom . $this->where;
//            echo $query;
            $stmt = $this->dbconn->query($query);
            return $stmt;
        } elseif ($this->update != null) {
            $query = $this->update . $this->set . $this->where;
//            echo $query;
            $stmt = $this->dbconn->query($query);
            return $stmt;
        }
    }


    public function insertInto($tableName, $columns)
    {
        $this->insertInto = 'INSERT INTO ' . $tableName . ' (';
        foreach ($columns as $column) {
            $this->insertInto .= '`' . $column . '`, ';
        }
        $this->insertInto = substr($this->insertInto, 0, -2) . ') ';

        return $this;
    }

    public function values()
    {
        $this->values = 'VALUES (';
        $vars = func_get_args();

        if (gettype($vars) == 'array') {
            $vars = $vars[0];
        }

        foreach ($vars as $var) {
            if (gettype($var) == 'integer') {
                $this->values .= $var . ', ';
            } elseif (gettype($var) == 'NULL') {
                $this->values .= 'null' . ', ';
            } else {
                $this->values .= "'" . $var . "', ";
            }
        }
        $this->values = substr($this->values, 0, -2) . ') ';
        return $this;
    }

    public function deleteFrom($tableName)
    {
        $this->deleteFrom = 'DELETE FROM ' . $tableName . ' ';
        return $this;

    }

    public function update($tableName)
    {
        $this->update = 'UPDATE ' . $tableName . ' ';
        return $this;
    }

    public function set()
    {
        $this->set = 'SET ';

        $vars = func_get_args();
        if (gettype($vars[0]) == 'array') {
            $vars = $vars[0];
        }

        for ($i = 0; $i < count($vars); $i += 2) {
            $this->set .= '`' . $vars[$i] . '`= ';
            if (gettype($vars[$i + 1]) == 'string') {
                $this->set .= "'" . $vars[$i + 1] . "'";
            } else {
                $this->set .= $vars[$i + 1];
            }
            $this->set .= ', ';
        }
        $this->set = substr($this->set, 0, -2) . ' ';
        return $this;
    }

    public function clearQery()
    {
        $this->select = null;
        $this->count = null;
        $this->from = null;
        $this->where = null;
        $this->limit = null;
        $this->offset = null;
        $this->insertInto = null;
        $this->values = null;
        $this->update = null;
        $this->set = null;
    }
}
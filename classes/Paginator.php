<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 11/08/2017
 * Time: 00:24
 */

namespace classes;


class Paginator
{
    public $total_count;
    public $page;
    public $count_per_page;

    public $objects;

    public
        $prevPageNumber,
        $nextPageNumber;

    function __construct($class, $count_per_page, $page, $where = null, $orderByColumns = null)
    {
        if (!$page) {
            $page = 1;
        }
        $this->object_class = $class;
        $this->total_count = $class::objectsGetTotalCount($where);
        $this->count_per_page = $count_per_page;

        $this->max_page = (int)($this->total_count / $count_per_page);
        if ($this->total_count % $count_per_page > 0) {
            $this->max_page += 1;
        }

        $this->prevPageNumber = $page - 1;
        $this->nextPageNumber = $page + 1;

        $this->page = (int)$page;
        $this->first = $this->count_per_page * ($page - 1);
        $this->ends = $this->count_per_page * $page;

        if ($where == null) {
            $this->objects = $this->object_class::getAllObjects($this->first, $this->count_per_page, $orderByColumns);
        } else {
            $this->objects = $this->object_class::getAllObjectsByFields($where, $this->first, $this->count_per_page, $orderByColumns);
        }
    }

    function calculatePage()
    {
        $this->first = $this->count_per_page * ($page - 1);
        $this->ends = $this->count_per_page * $page;
        $this->objects = $this->object_class::getAllObjects($this->first, $this->count_per_page);

    }

    public function next()
    {
        if ($this->hasNext()) {
            $this->page += 1;
            $this->calculatePage();
        }

    }

    public function prev()
    {
        if ($this->hasPrev()) {
            $this->page -= 1;
            $this->calculatePage();
        }
    }

    public function hasNext()
    {
        if ($this->page < $this->max_page) {
            return true;
        } else {
            return false;
        }

    }

    public function hasPrev()
    {
        if ($this->page > 1) {
            return true;
        } else {
            return false;
        }

    }


}
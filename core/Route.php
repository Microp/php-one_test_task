<?php


/**
 *
 */
class Route
{
    public
        $settings,   // settings site
        $uri,        // current uri
        $app;        // current application

    function __construct($settings)
    {
        $this->settings = $settings;
        $this->uri = urldecode(preg_replace('/\?.*/iu', '', $_SERVER['REQUEST_URI']));
        $this->app = false;
        $this->process_path();
        $this->process_controller();
    }

    public function process_path()
    {
        $core_urls_patten = require 'urls.php';

        foreach ($core_urls_patten as $pattern => $app) {

            if (preg_match($pattern, $this->uri)) {

                $iterable_urls = require(BASE_DIR . '/apps/' . $app . '/urls.php');

                foreach ($iterable_urls as $pattern => $method) {
                    if (preg_match($pattern, $this->uri)) {
                        $this->app = array(
                            'name' => $app,
                            'param' => array(
                                'pattern' => $pattern,
                                'method' => $method
                            )
                        );
                        break(2);
                    }
                }
            }
        }

        if ($this->app === false) {
            echo 'Page not found';
            exit();
        }
    }

    public function process_controller()
    {
        if ($this->app || is_array($this->app)) {
            require(BASE_DIR . '/apps/' . $this->app['name'] . '/Controller_' . $this->app['name'] . '.php');
            $controller_name = 'Controller_' . $this->app['name'];
            $this->app_controller = new $controller_name($this->app['name']);

            $this->app_controller->{$this->app['param']['method']}();

        }

    }


    public static function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not found');
        header('Status: 404 Not found');
        header('Location:' . $host . '404');
    }
}

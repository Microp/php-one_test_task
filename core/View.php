<?php
/**
 * Created by PhpStorm.
 * User: alexander
 * Date: 07/08/2017
 * Time: 22:34
 */

//namespace core;


class View
{
    private $app_name;

    function __construct($app_name)
    {
        $this->app_name = $app_name;
    }

    function generate($content_view, $template_view, $data = null)
    {
        $content_view = BASE_DIR . '/apps/'. $this->app_name . '/'. $content_view;
        include $template_view;
    }

}
<?php

return array(
    '#sign_in/*$#' => 'sign_in',
    '#sign_up$/*#' => 'sign_up',
    '#sign_out$/*#' => 'sign_out',
);

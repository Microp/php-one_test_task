  <div class="container">
    <div class="row justify-content-center text-center">
      <h1>Registration</h1>
    </div>
  </div>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-6 form-group">
        <form action="sign_up" method="post">
          <input type="text" class="form-control" name="username" placeholder="Username"
                 value="<?php echo $data['form']['username']; ?>">
          <input type="text" class="form-control" name="email" placeholder="Email"
                 value="<?php echo $data['form']['email']; ?>">
          <input type="password" class="form-control " name="password" placeholder="Pass"
                 value="<?php echo $data['form']['password']; ?>">
          <input type="password" class="form-control" name="password_2" placeholder="Pass">

          <button type="submit" class="btn btn-info" name="do_sign_up">Registration</button>
        </form>
      </div>
    </div>
  </div>

<?php if (!empty($data['errors'])): ?>
  <div class="container">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <button type="button" data-dismiss="alert" class="close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>Danger! </strong><?php echo $data['errors'][0] ?>
    </div>
  </div>
<?php endif; ?>

  <div class="row">
    <div class="col-10">
    </div>
    <div class="col-2">
      <a href="/" class="btn btn-outline-success"> Back to main page</a>
    </div>
  </div>
<?php

echo 'This page is neded for registrate users';

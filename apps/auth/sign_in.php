<?php //require BASE_DIR . 'apps/static/header.php'; ?>


<?php if (!empty($data['errors'])): ?>
  <div class="container">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <button type="button" data-dismiss="alert" class="close">
        <span aria-hidden="true">&times;</span>
      </button>
      <strong>Danger! </strong><?php echo $data['errors'][0] ?>
    </div>
  </div>
<?php endif; ?>
<div class="container">
  <div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2">
      <a href="/" class="btn btn-outline-success">Back to main page</a>
    </div>
  </div>
</div>
<?php
echo 'This page is needed for authorize users';
?>

<?php //require BASE_DIR . 'apps/static/footer.php'; ?>

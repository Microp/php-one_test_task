<?php

use classes\models\User;

class Controller_auth extends Controller
{
    function sign_in()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        $form = $_POST;
        $errors = array();

        if (isset($form['do_sign_in'])) {
            if ($form['username'] == '') {
                $errors[] = 'Input your username';
            }
            if ($form['password'] == '') {
                $errors[] = 'Input password';
            }

            if (empty($errors)) {
                $error = User::signIn($form['username'], $form['password']);

                if (!$errors) {
                    header('Location: /', true, 303);
                } else {
                    $errors[] = $error;
                }

            }
        }

        $data = array(
            'app' => 'auth',
            'errors' => $errors
        );

        $this->view->generate('sign_in.php', BASE_DIR . '/apps/template_page.php', $data);

    }

    function sign_up()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (isset($_SESSION['user'])) {
            header('Location: /', true, 304);
        }

        $form = $_POST;
        $errors = array();
        if (isset($form['do_sign_up'])) {
            if (!empty($form)) {
                if ($form['username'] == '') {
                    $errors[] = 'Input name';
                }
                if ($form['email'] == '') {
                    $errors[] = 'Input Email';
                }
                if ($form['password'] == '') {
                    $errors[] = 'Input password';
                }
                if ($form['password_2'] == '') {
                    $errors[] = 'Input second passord';
                }
                if ($form['password_2'] != $form['password']) {
                    $errors[] = 'Password mismatch';
                }
            }

            if (empty($errors) and !empty($form)) {
                $user = new User($form['username'], $form['email'], $form['password']);
                $error = $user->create();

                if ($error != false) {
                    $errors[] = $error;
                } else {
                    User::signIn($user->username, $form['password']);
                    header('Location: /', true, 302);
                }
            }
        }

        $data = array(
            'app' => 'auth',
            'form' => $form,
            'errors' => $errors
        );

        $this->view->generate('sign_up.php', BASE_DIR . '/apps/template_page.php', $data);
    }

    function sign_out()
    {
        session_start();
        session_destroy();

        header('Location: /', true, 302);
    }

}
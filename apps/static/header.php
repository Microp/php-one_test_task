<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Php one</title>
  <link rel="stylesheet" href="../../static/bootstrap/css/bootstrap.min.css">
  <script src="../../static/js/jquery/test/data/jquery-1.9.1.js"></script>
  <script src="../../static/bootstrap/js/bootstrap.min.js"></script>
  <script src="../../static/js/own/functions.js"></script>
</head>
<body>


<nav class="navbar navbar-toggleable-md navbar-inverse bg-primary">

  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
          data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
          aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <a class="navbar-brand" href="/">Php_one + slonik</a>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">

      <li class="nav-item">
        <a class="nav-link" href="/">My blog</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="users">Users</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/template">News feed</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/test">Test</a>
      </li>
    </ul>

      <?php if (!isset($_SESSION['user'])): ?>
        <form action="/auth/sign_in" class="form-inline my-2 my-lg-0" method="post">
          <div class="form-group">
            <input type="text" class="form-control mr-sm-2" placeholder="Username" name="username">
            <input type="password" class="form-control mr-sm-2" placeholder="Password" name="password">
          </div>
          <div>
            <button type="submit" name="do_sign_in" class="btn btn-info ">Sign in</button>
            <a href="/auth/sign_up" class="btn btn-info">Sign up</a>
          </div>
        </form>
      <?php else: ?>
        <h1 class="navbar-brand mb-0"><?php echo $_SESSION['user']->username; ?></h1>
        <a href="/auth/sign_out" class="btn btn-info">Sign out</a>
      <?php endif; ?>


  </div>
</nav>

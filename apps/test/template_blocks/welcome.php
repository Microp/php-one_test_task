<?php

use classes\DbConnection;


$db = DbConnection::getInstance();

$stmt = $db->old_select("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_TYPE='BASE TABLE'");




if (empty($stmt)) {
    echo "<p>There are no tables in database</p>";
    require('./database/create_tables_sql/users.php');


} else {
    echo "<div class='row'>";
    echo "<p>Database has the following tables:</p>";
    echo "</div>";
    echo "<div class='row'>";
    echo "<ul>";
    while ($row = $stmt->fetch(PDO::FETCH_LAZY)) {
        echo "<li>$row[0]</li>";
    }
    echo "</ul>";
    echo "</div>";
}
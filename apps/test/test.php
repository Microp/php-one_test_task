<?php
use classes\DbConnection;
use classes\models\Human;
use classes\models\User;
use classes\models\Article;
use classes\models\Comment;

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (!empty($_POST)) {
    $data = $_POST;

    $data['article_id'] = (is_numeric($data['article_id']) ? (int)$data['article_id'] : null);
    $data['user_id'] = (is_numeric($data['user_id']) ? (int)$data['user_id'] : null);


    $comment = new Comment($data['text'], $data['article_id'], $data['user_id']);
    $comment->create();
}

$articles = Article::getAllObjects();

foreach ($articles as $article) {


    ?>
  <form action="" method="post">
    <p>Article write by <?= $article->user->username ?></p>
    <p><?= $article->text ?></p>
    <input type="hidden" name="article_id" value="<?= $article->id ?>">
    <input type="hidden" name="user_id" value="<?= $_SESSION['user']->id ?>">
    <div style="margin-left: 100px;">
        <?php
        $comments = Comment::getAllObjectsByFields(['article_id', '=', $article->id]);
        foreach ($comments as $comment) {
            ?>


          <p><?php if ($comment != null) {
                echo $comment->user->username;
            } else {
                echo 'Anonim';
            } ?><p>
          <p><?= $comment->text ?></p>
        <?php } ?>


      <p>Write your comment for this article</p>
      <input type="text" name="text" id="">
      <input type="submit" value="do_comment">
    </div>
  </form>

    <?php
}

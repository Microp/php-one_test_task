<div class="container">
  <div class="col-md-10 offset-sm-1">
    <p class="text-info">Article write by <?= $data['article']->user->username ?> </p>
    <p class="text-center"><?= $data['article']->text ?></p>


      <?php foreach ($data['paginator']->objects as $comment) { ?>
        <p class="text-info"><?= $comment->user->username ?> </p>
        <p class="text"><?= $comment->text ?></p>
        <hr>
      <?php } ?>

    <nav aria-label="">
      <ul class="pagination">
        <li class="page-item <?php if (!$data['paginator']->hasPrev()) {
            echo 'disabled';
        } ?>">
          <a class="page-link" href="/comments?article_id=<?=$data['article']->id . '&page=' . $data['paginator']->prevPageNumber ?>"
             tabindex="-1">Previous</a>
        </li>
        <li class="page-item active">
          <a class="page-link" href="#"><?= $data['paginator']->page ?> <span
                    class="sr-only">(current)</span></a>
        </li>
        <li class="page-item <?php if (!$data['paginator']->hasNext()) {
            echo 'disabled';
        } ?>">
          <a class="page-link" href="/comments?article_id=<?=$data['article']->id . '&page=' . $data['paginator']->nextPageNumber ?>">Next</a>
        </li>
      </ul>
    </nav>

    <form action="/comments?article_id=<?= $data['article']->id ?>"
          class="" method="post">
      <input type="hidden" name="article_id" value="<?= $data['article']->id ?>">
      <input type="hidden" name="user_id" value="<?= $_SESSION['user']->id ?>">

      <div class="form-group">
        <input type="text" class="form-control" name="text" id="">
      </div>
      <input type="submit" class="btn btn-info" name="do_comment" value="Comment this message">

    </form>

  </div>
</div>

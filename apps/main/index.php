<div class="container" style="margin-top: 2em;">
  <div class="row justify-content-center">
    <h1>Here will be main content of my guest book</h1>

  </div>
</div>


<div class="container">
  <div class="row justify-content-center">
    <button onclick="openMessageForm()" class="btn btn-success">Write Message</button>
  </div>
</div>

<?php include(BASE_DIR . '/apps/main/static_blocks/form_message.php'); ?>


<div class="container">
  <div class="col-md-10 offset-md-1">
    <form action="" method="post">
        <?php foreach ($data['articles'] as $article) { ?>
            <?php if ($article->user->username) : ?>
            <p class="text-info">Article write by <?= $article->user->username ?> </p>
                <?php else: ?>
            <p class="text-info">Article write by <?= $article->temp_name ?> <span style="color: red">(Anonym)</span> </p>
                <?php endif; ?>
          <p class="text-center"><?= $article->text ?></p>
          <div class="row">
            <div class="col-md-10"></div>
            <div class="col-md-2">
              <a href="/comments?article_id=<?= $article->id ?>" class="btn btn-info">Comment this message</a>
            </div>
          </div>
        <?php } ?>


    </form>
  </div>
</div>


<div class="container">
  <div class="row justify-content-center">


    <nav aria-label="">
      <ul class="pagination">
        <li class="page-item <?php if (!$data['article_paginator']->hasPrev()) {
            echo 'disabled';
        } ?>">
          <a class="page-link" href="/?page=<?= $data['article_paginator']->prevPageNumber ?>" tabindex="-1">Previous</a>
        </li>
        <li class="page-item active">
          <a class="page-link" href="#"><?= $data['article_paginator']->page ?> <span
                    class="sr-only">(current)</span></a>
        </li>
        <li class="page-item <?php if (!$data['article_paginator']->hasNext()) {
              echo 'disabled';
          } ?>"  >
        <a class="page-link" href="/?page=<?= $data['article_paginator']->nextPageNumber ?>">Next</a>
        </li>
      </ul>
    </nav>

  </div>
</div>

<div class="container bg-faded" style="margin-top: 2em; margin-bottom: 2em; display: none;" id="form_message">
  <div class="form-group">

    <form action="" method="post">
      <input type="hidden" name="user_id" value="<?= $_SESSION['user']->id ?>">
      <p>Input your message</p>
      <input type="text" class="form-control" name="text" id="">
        <?php if (!isset($_SESSION['user'])): ?>
          <p>Message will send as anonymous message
          <p>You can define your name
            <input type="text" name="temp_name" id=""></p>
        <?php endif; ?>

      <button type="submit" class="btn btn-success" style="margin-top: 5px;" name="do_form_message"> Send message
      </button>

    </form>

  </div>
</div>
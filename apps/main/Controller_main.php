<?php


use classes\models\User;
use classes\Paginator;
use classes\models\Article;
use classes\models\Comment;

class Controller_main extends Controller
{

    public function index()
    {
        $data = array(
            'app' => $this->app_name,
        );

        if (!empty($_POST)) {
            $form = $_POST;

            $form['user_id'] = (is_numeric($form['user_id']) ? (int)$form['user_id'] : null);

            $article = new Article('Guest message', $form['text'], $form['user_id'], $form['temp_name']);
            $article->create();
        }

        $paginator = new Paginator(
            Article::class,
            3,
            $_GET['page'],
            null,
            '-id'
        );

        $data['article_paginator'] = $paginator;
        $data['articles'] = $paginator->objects;


        $this->view->generate('index.php', BASE_DIR . '/apps/template_page.php', $data);
    }

    public function comments_articles()
    {
        $data = array(
            'app' => $this->app_name,
        );

        if (!empty($_POST)) {
            $form = $_POST;

            $form['article_id'] = (is_numeric($form['article_id']) ? (int)$form['article_id'] : null);
            $form['user_id'] = (is_numeric($form['user_id']) ? (int)$form['user_id'] : null);

            $comment = new Comment($form['text'], $form['article_id'], $form['user_id']);
            $comment->create();
        }

        $paginator = new Paginator(
            Comment::class,
            5,
            $_GET['page'],
            array('article_id', '=', $_GET['article_id']),
            '-id'
        );

        $data['article'] = Article::getObjectById($_GET['article_id']);
        $data['paginator'] = $paginator;

        $this->view->generate('comments.php', BASE_DIR . '/apps/template_page.php', $data);

    }

    public function users()
    {
        $paginator = new Paginator(
            User::class,
            2,
            $_GET['page'],
            null,
            '-id'
        );
        $data = array(
            'app' => $this->app_name,
            'users' => $paginator,
        );

        $this->view->generate('users.php', BASE_DIR . '/apps/template_page.php', $data);
    }

}
# README #

класс DbConnection - является query builder 'ом и полностью отвечает за работу с базой данных

Модели расположены в папке classes/models

В папке core содержится ядро сайта:
	route - класс отвечающий за роутинг, выборку необходимого приложения и его контроллера для последующего выполнения по URL
	controller - базовый класс контроллеров
	view - базовый класс представлений
	
Роутинг реализуется следующим образом - сначала URL проверяется на соответствие приложению, а после на соответствия соответствующего метода контроллера в приложении

Сейчас на сайте несколько приложений:
	auth
	main
	test
	
В каждом приложении реализованы: 
	urls
	app_contorller
	html_templates
	
Для развертывания всего сайта вы можете скачать 
конфигурации docker образов из репозитория - https://bitbucket.org/Microp/web_stack-docker-deployment
Для запуска контейнеров необхомо будет:
	изменить ключевое слово (path) в папке docker-compose.yml на расположение содержимого выших файлов
далее запустить команду из папки с docker-compose.yml:
	docker-compose up -d
Далее в браузере необходимо перейти на страницу localhost/test для установки всех таблиц в базе данных
<?php

require_once('configs/variables_conf.php');
require_once('classes/DbConnection.php');


// FIELDS
require_once('classes/models/fields/Field.php');
require_once('classes/models/fields/IntegerField.php');
require_once('classes/models/fields/CharField.php');
require_once('classes/models/fields/DateField.php');
require_once('classes/models/fields/PasswordField.php');


// MODELS
require_once('classes/models/Model.php');
require_once('classes/models/Human.php');
require_once('classes/models/User.php');
require_once('classes/models/Article.php');
require_once('classes/models/Comment.php');


require_once('classes/Paginator.php');


// OTHER
require_once('core/Controller.php');
require_once('core/Route.php');
require_once('core/View.php');

$db = \classes\DbConnection::getInstance();

$query = \classes\models\User::createTable();
$stmt = $db->old_select($query);

$query = \classes\models\Human::createTable();
$stmt = $db->old_select($query);


$query = \classes\models\Article::createTable();
$stmt = $db->old_select($query);